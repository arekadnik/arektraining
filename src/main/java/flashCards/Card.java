package flashCards;

import java.util.Objects;

public class Card {
    String definition;
    String term;

    public Card(String definition, String term) {
        this.definition = definition;
        this.term = term;
    }

    public Card() {
    }

    public String getDefinition() {
        return definition;
    }

    public void setDefinition(String definition) {
        this.definition = definition;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Card card = (Card) o;
        return Objects.equals(definition, card.definition) &&
                Objects.equals(term, card.term);
    }

    @Override
    public int hashCode() {
        return Objects.hash(definition, term);
    }

    @Override
    public String toString() {
        return "Card{" +
                "definition='" + definition + '\'' +
                ", term='" + term + '\'' +
                '}';
    }
}