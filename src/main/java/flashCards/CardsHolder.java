package flashCards;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class CardsHolder {
    Scanner scanner = new Scanner(System.in).useDelimiter("\\n");
    Map<String, String> cardMap = new LinkedHashMap<>();

    public void start() {
        while (true) {
            System.out.println("Input the action (add, remove, import, export, ask, exit):");
            String action = scanner.next();
            if ("add".equals(action)) {
                addCard();
            } else if ("remove".equals(action)) {
                removeCard();
            } else if ("import".equals(action)) {
                importCards();
            } else if ("export".equals(action)) {
                exportCards();
            } else if ("ask".equals(action)) {
                ask();
            } else if ("exit".equals(action)) {
                exit();
                break;
            }
            System.out.println();
        }
    }

    private void importCards() {
        System.out.println("File name");
        String fileName = scanner.next();
        File file = new File("/Users/arek/IdeaProjects/Flashcards/Flashcards/task/src/flashcards",
                fileName + ".txt");
        if (!file.exists()) {
            System.out.println("File not found.");
            return;
        }
        Scanner sc = null;
        try {
            sc = new Scanner(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        int count = 0;
        while (sc.hasNext()) {
            System.out.println(sc.nextLine());
            count++;
        }
        System.out.println(count + " cards have been loaded.");
    }


    private void exportCards() {
        System.out.println("File name");
        String fileName = scanner.next();
        int count = 0;
        for (Map.Entry<String, String> entry : cardMap.entrySet()) {
            File file = new File("/Users/arek/IdeaProjects/Flashcards/Flashcards/task/src/flashcards",
                    fileName + ".txt");
            try {
                FileWriter fileWriter = new FileWriter(file, true);
                fileWriter.write(entry.getKey() + " " + entry.getValue() + "\n");
                fileWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            count++;
        }
        System.out.println(count + " cards have been saved.");
    }




    void addCard() {
        System.out.println("The card:");
        String term = scanner.next();
        if (cardMap.containsKey(term)) {
            System.out.println("The card " + "\"" + term + "\"" + " already exists.");
            return;
        }
        System.out.println("The definition of the card: ");
        String definition = scanner.next();
        if (cardMap.containsValue(definition)) {
            System.out.println("The definition " + "\"" + definition + "\"" + " already exists.");
            return;
        }
        cardMap.put(term, definition);
        System.out.println("The pair (" + "\""+term +"\""+ ":" +"\""+ definition +"\""+ ") has been added.");

    }
    private void removeCard() {
        System.out.println("The card:");
        String term = scanner.next();
        if (!cardMap.containsKey(term)) {
            System.out.println("Can't remove \"" + term + "\": there is no such card.");
            return;
        }
        cardMap.remove(term);
        System.out.println("The card has been removed.");
    }

    private void exit() {
        System.out.println("Bye Bye");
    }

    private void ask() {
        List<Card> cardList = new ArrayList<>();
        for (Map.Entry<String, String> entry : cardMap.entrySet()) {
            cardList.add(new Card(entry.getValue(), entry.getKey()));
        }
        System.out.println("How many times to ask?");
        int count = scanner.nextInt();
        int lenght = Math.min(count,cardList.size());
        Random random = new Random();
        int number = random.nextInt(lenght);
        for (int i = 0; i < lenght; i++) {
            System.out.println("Print the definition of \"" + cardList.get(number).getTerm() + "\":");
            String answer = scanner.next();
            if (cardList.get(i).getDefinition().equalsIgnoreCase(answer)) {
                System.out.println("Correct answer");
            } else if (cardMap.containsValue(answer)) {
                System.out.println("Wrong answer. The correct one is \"" + cardList.get(i).getDefinition() + "\"" +
                        ", you've just written the definition of \"" + getKey(cardMap, answer) + "\".\n");
            } else {
                System.out.println("Wrong answer. The correct one is \"" + cardList.get(i).getDefinition() + "\".");
            }
        }
    }

    public String getKey(Map<String, String> map, String answer) {
        for (Map.Entry<String, String> entry : map.entrySet()) {
            if (entry.getValue().equals(answer)) {
                return entry.getKey();
            }
        }
        return null;
    }

//    public void DefinitionChecking(Scanner scanner, ArrayList<Card> cardsList, int numberOfCards) {
//        Map<String, String> cardMap = parseListToMap(cardsList);
//        int counter = 0;
//        do {
//            System.out.println("Print the definition of " + "\"" + cardsList.get(counter).getTerm() + "\"");
//            String definitionAnswer = scanner.next();
//            if (cardsList.get(counter).getDefinition().equalsIgnoreCase(definitionAnswer)) {
//                System.out.println("Correct answer.");
//            } else if (cardMap.containsValue(definitionAnswer)) {
//                System.out.println("Wrong answer. The correct one is \"" + cardsList.get(counter).getDefinition() + "\"" +
//                        ", you've just written the definition of \"" + getKey(cardMap, definitionAnswer) + "\".\n");
//            } else {
//                System.out.println("Wrong answer. The correct one is \"" + cardsList.get(counter).getDefinition() + "\".");
//            }
//            counter++;
//        } while (counter < numberOfCards);
//    }


//    public String checkArrayTerm(ArrayList<Card> arrayList, Scanner scanner, String term) {
//        Map<String, String> parseListToMap = parseListToMap(arrayList);
//        while (parseListToMap.containsKey(term)) {
//            System.out.println("The card " + "\"" + term + "\"" + " already exists. Try again:");
//            term = scanner.next();
//        }
//        return term;
//    }
//
//    public String checkArrayDefinition(ArrayList<Card> arrayList, Scanner scanner, String def) {
//        Map<String, String> parseListToMap = parseListToMap(arrayList);
//        while (parseListToMap.containsValue(def)) {
//            System.out.println("The definition " + "\"" + def + "\"" + " already exists. Try again:");
//            def = scanner.next();
//        }
//        return def;
//    }

//    private Map<String, String> parseListToMap(List<Card> cardList) {
//        Map<String, String> cardMap = new LinkedHashMap<>();
//        for (Card c : cardList) {
//            cardMap.put(c.term, c.definition);
//        }
//        return cardMap;
//    }


}

