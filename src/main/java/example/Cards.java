package example;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Cards {
    private final Map<String, Card> terms;
    private final Map<String, Card> definitions;

    public Cards() {
        terms = new LinkedHashMap<>();
        definitions = new LinkedHashMap<>();
    }

    void add(Card card) {
        terms.put(card.term, card);
        definitions.put(card.definition, card);
    }

    boolean containsTerm(String term) {
        return terms.containsKey(term);
    }

    boolean containsDefinition(String definition) {
        return definitions.containsKey(definition);
    }

    Card getByDefinition(String definition) {
        return definitions.get(definition);
    }

    List<Card> values() {
        return new ArrayList<>(terms.values());
    }

    public void removeByTerm(String term) {
        Card removedCard = terms.remove(term);
        definitions.remove(removedCard.definition);
    }
}
