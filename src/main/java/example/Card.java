package example;

import java.io.Serializable;

public class Card implements Serializable {
    final String term;
    final String definition;

    private Card(String term, String definition) {
        this.term = term;
        this.definition = definition;
    }

    static Card of(String term, String definition) {
        return new Card(term, definition);
    }

    boolean right(String answer) {
        return definition.equals(answer);
    }

    @Override
    public String toString() {
        return term +
                ":" +
                definition;
    }
}
