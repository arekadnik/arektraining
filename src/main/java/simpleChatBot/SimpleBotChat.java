package simpleChatBot;

import java.util.Scanner;

public class SimpleBotChat {
    final static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {

        greet();
        remindName();
        guessAge();
        count();
        Asking();
    }

    public static void Asking() {
        int choose;
        System.out.println("Let's test your programming knowledge.");
        System.out.println("Why do we use methods?");
        System.out.println("1.  To repeat a statement multiple times.");
        System.out.println("2.  To decompose a program into several small subroutines.");
        System.out.println("3.  To determine the execution time of a program.");
        System.out.println("4.  To interrupt the execution of a program.");
        do{
            choose = scanner.nextInt();
            if(choose != 2){
                System.out.println("Please, try again.");
            }else {
                System.out.println("Congratulations, have a nice day!");
            }
        }while (choose != 2);
    }

    public static void count() {
        System.out.println("Now I will prove to you that I can count to any number you want.");
        int provideNumber = scanner.nextInt();
        for (int i = 0; i <= provideNumber; i++) {
            System.out.println(i + "!");
        }
    }

    public static void guessAge() {
        System.out.println("Let me guess your age.");
        System.out.println("Say me remainders of dividing your age by 3, 5 and 7.");
        int number1 = scanner.nextInt();
        int number2 = scanner.nextInt();
        int number3 = scanner.nextInt();
        int age = (number1 * 70 + number2 * 21 + number3 * 15) % 105;
        System.out.println("Your age is " + age + " that's a good time to start programming!");
    }

    public static void remindName() {
        String name = scanner.nextLine();
        System.out.println("What a great name you have, " + name);
    }

    public static void greet() {
        System.out.println("Hello! My name is Arnold");
        System.out.println("I was created in 2020");
        System.out.println("Please, remind me your name.");
    }
}
